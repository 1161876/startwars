//
//  CharactersTableView.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit

extension CharactersView: UITableViewDelegate, UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return listOfCharacters.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterCell") as! CharacterCell
		let character = listOfCharacters[indexPath.row]
		cell.setData(name: character.name!, numberOfVeicules: character.veiculesUrl!.count, species: character.species?.name ?? "")
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 90
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		selectedCharacter = listOfCharacters[indexPath.row]
		performSegue(withIdentifier: "charactersToDetail", sender: self)
	}
}
