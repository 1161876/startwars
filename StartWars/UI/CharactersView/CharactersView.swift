//
//  CharactersView.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CharactersView: UIViewController {
	
	@IBOutlet weak var charactersTableView: UITableView!
	@IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
	
	var listOfCharacters: [Character] = []
	var selectedCharacter: Character = Character()
	var refresher: UIRefreshControl!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		loadingIndicator.startAnimating()
		registerAllTableViewCells()
		
		getCharacters()
		
		//add a pull to refresh animation
		refresher = UIRefreshControl()
		refresher.addTarget(self, action: #selector(getCharacters), for: UIControl.Event.valueChanged)
		charactersTableView.addSubview(refresher)
    }
	
	@objc func getCharacters(){
		
		//check if internet connection is available
		if (Reachability.isConnectedToNetwork()){
			//get all the characters
			API().getCharacters() { [weak self] characters in
				
				var listOfCharacters = characters as? [Character] ?? []
				
				if (listOfCharacters.count != 0){
					
					//dictionary only with the unique species
					var speciesDictionary = [String:Species] ()
					
					//get only the unique values to fetch from the data base
					for n in 0...listOfCharacters.count - 1 {
						let character = listOfCharacters[n]
						if (character.speciesUrl!.count > 0){
							speciesDictionary[character.speciesUrl![0]] = Species()
						}
					}
					
					var cont = 0
					
					//fetch the unique species
					for key in speciesDictionary.keys{
						API().getCharacterSpecies(specieUrl: key) { [weak self] species in
							let species = species as? Species ?? nil
							if (species != nil){
								speciesDictionary[key] = species!
							}
							cont +=  1
							
							if (cont == speciesDictionary.count){
								self!.listOfCharacters = listOfCharacters
								self!.updatedValues(species: speciesDictionary)
							}
						}
					}
					
				}else{
					print("no data to show")
				}
			}
		}else{
			let alert = UIAlertController(title: "No internet!", message: "Please reconnect to the internet and then press retry", preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
				self.getCharacters()
			}))
			
			self.present(alert, animated: true)
		}
		
	}
	
	
	func updatedValues(species: [String:Species]){
		
		//update species information in the characters array
		if(listOfCharacters.count > 0){
			for n in 0...listOfCharacters.count - 1{
				self.listOfCharacters[n].species = species[listOfCharacters[n].speciesUrl![0]]
			}
		}
		charactersTableView.reloadData()
		loadingIndicator.stopAnimating()
		refresher.endRefreshing()
	}
	
	func registerAllTableViewCells(){
		charactersTableView.register(UINib.init(nibName: "CharacterCell", bundle: nil), forCellReuseIdentifier: "CharacterCell")
	}
	
	//send selected character to Character details view
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "charactersToDetail") {
			let vc = segue.destination as! CharacterDetailsView
			vc.character = self.selectedCharacter
		}
	}

}
