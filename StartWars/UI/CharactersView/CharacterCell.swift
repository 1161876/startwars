//
//  CharacterCell.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit

class CharacterCell: UITableViewCell {

	@IBOutlet weak var name: UILabel!
	@IBOutlet weak var numberOfVeicules: UILabel!
	@IBOutlet weak var species: UILabel!
	
	func setData(name: String, numberOfVeicules: Int, species: String){
		self.name.text = name
		self.numberOfVeicules.text = "Has " + String(numberOfVeicules) + " vehicles"
		self.species.text = species
	}
	
}
