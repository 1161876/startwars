//
//  AboutView.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit

class AboutView: UIViewController {
	
	@IBOutlet weak var devImage: UIImageView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }

	@IBAction func linkedInPressed(_ sender: UIButton) {
		if let url = NSURL(string: "https://www.linkedin.com/in/tiagoandresantos"){
			UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
		}
	}
}
