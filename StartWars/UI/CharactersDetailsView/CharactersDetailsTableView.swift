//
//  CharactersDetailsTableView.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation
import UIKit

extension CharacterDetailsView: UITableViewDataSource, UITableViewDelegate{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if (self.character.veiculesUrl?.count == 0 ){
			self.veiculesLabel.text = "No vehicles"
		}
		loadingIndication.stopAnimating()
		return character.veicules?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterDetailsCell") as! CharacterDetailsCell
		cell.name.text = character.veicules![indexPath.row].name
		return cell
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 50
	}
	
}
