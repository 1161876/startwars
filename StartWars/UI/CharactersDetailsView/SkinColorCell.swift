//
//  SkinColorCell.swift
//  StartWars
//
//  Created by Tiago  Santos on 26/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit

class SkinColorCell: UICollectionViewCell {
	@IBOutlet weak var skinColor: UIView!
}
