//
//  CharacterDetailsCell.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit

class CharacterDetailsCell: UITableViewCell {
	@IBOutlet weak var name: UILabel!
}
