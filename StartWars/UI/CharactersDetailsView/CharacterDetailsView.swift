//
//  CharacterDetailsView.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CharacterDetailsView: UIViewController {
	
	@IBOutlet weak var veiculesLabel: UILabel!
	@IBOutlet weak var loadingIndication: NVActivityIndicatorView!
	@IBOutlet weak var skinColorLabel: UILabel!
	@IBOutlet weak var planetLabel: UILabel!
	@IBOutlet weak var genderLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	
	@IBOutlet weak var characterDetailsTableView: UITableView!
	
	var character = Character()
	var characterName = ""
	var skinColors:[String] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		loadingIndication.startAnimating()
		registerAllTableViewCells()
		loadData()
    }
	
	func loadData(){
		
		//check if internet connection is available
		if (Reachability.isConnectedToNetwork()){
			if (character.name != nil){
				characterName = character.name!
			}
			getPlanet()
			getVehicles()
		}else{
			let alert = UIAlertController(title: "No internet!", message: "Please reconnect to the internet and then press retry", preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
				self.loadData()
			}))
			
			self.present(alert, animated: true)
		}
	}
	
	func registerAllTableViewCells(){
		characterDetailsTableView.register(UINib.init(nibName: "CharacterDetailsCell", bundle: nil), forCellReuseIdentifier: "CharacterDetailsCell")
	}
	
	@IBAction func backButtonPressed(_ sender: Any) {
		performSegue(withIdentifier: "detailsToTabBar", sender: self)
	}
	
	@IBAction func googleClicked(_ sender: UIButton) {
		let baseuUrl = "https://www.google.com/search?q=Star%20Wars%20"
		characterName = characterName.replacingOccurrences(of: " ", with: "%20")
		let url = baseuUrl + characterName
		
		if let url = NSURL(string: url){
			UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
		}
		
	}
	
	func getVehicles(){
		if (character.veiculesUrl!.count > 0){
			var listOfVeicules:[Vehicle] = []
			for n in 0...character.veiculesUrl!.count - 1 {
				API().getVehicles(vehiclesUrl: character.veiculesUrl![n]) { [weak self] vehicle in
					let vehicle = vehicle as! Vehicle
					if (vehicle.name != nil){
						listOfVeicules.append(vehicle)
					}
					if(n == self!.character.veiculesUrl!.count - 1){
						self!.character.veicules = listOfVeicules
						self!.characterDetailsTableView.reloadData()
					}
				}
			}
		}
	}
	
	func getPlanet(){
		if (character.planet == nil){
			API().getPlanet(planetUrl: character.planetUrl!) { [weak self] planet in
				self!.character.planet = (planet as! String)
				self!.loadCharaterInformation()
			}
		}else{
			self.loadCharaterInformation()
		}
	}
	
	func loadCharaterInformation(){
		skinColorLabel.text = "Skin color: " + character.skinColor!.capitalized
		planetLabel.text = "Planet: " + character.planet!.capitalized
		genderLabel.text = "Gender: " + character.genre!.capitalized
		nameLabel.text = "Name: " + character.name!.capitalized
	}
	
}
