//
//  SkinColorsColection.swift
//  StartWars
//
//  Created by Tiago  Santos on 26/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation
import UIKit

extension CharacterDetailsView: UICollectionViewDelegate, UICollectionViewDataSource{
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		//if the skin color are seperated with a comma and all of them are known, they will be displayed correctly in the colection view
		let skinColorsList = self.character.skinColor!.split(separator: ",")
		for color in skinColorsList{
			let skinColor = color.trimmingCharacters(in: .whitespaces)
			self.skinColors.append(String(skinColor))
		}
		
		return self.character.skinColor!.split(separator: ",").count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorCell", for: indexPath) as! SkinColorCell
		
		//check if color is known
		if let color = UIColor(named: String(self.skinColors[indexPath.row])){
			cell.skinColor.backgroundColor = color
		}else{
			cell.skinColor.backgroundColor = UIColor.white
		}
		
		return cell
	}
	
	
}
