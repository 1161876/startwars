//
//  Species.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation

class Species{
	
	var name: String?
	
	init(name: String) {
		self.name = name
	}
	
	init() {}
}

