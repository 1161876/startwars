//
//  Character.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation

class Character{
	
	var name: String?
	var speciesUrl: [String]?
	var species: Species?
	var genre: String?
	var planetUrl: String?
	var planet: String?
	var skinColor: String?
	var veiculesUrl: [String]?
	var veicules: [Vehicle]?
	
	init(name: String, speciesUrl: [String], genre: String, planetUrl: String, skinColor: String, veiculesUrl: [String],species: Species?, veicules: [Vehicle]?, planet: String? ) {
		self.name = name
		self.speciesUrl = speciesUrl
		self.genre = genre
		self.planetUrl = planetUrl
		self.skinColor = skinColor
		self.veiculesUrl = veiculesUrl
		self.species = species
		self.veicules = veicules
		self.planet = planet
	}
	
	init() {}
	
}
