//
//  Veicules.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation

class Vehicle{

	var name: String?
	var model: String?
	var crew: String?
	var passengers: String?
	
	init(name: String, model: String, crew: String, passengers: String) {
		self.name = name
		self.model = model
		self.crew = crew
		self.passengers = passengers
	}
	
	init() {}
}
