//
//  API.swift
//  StartWars
//
//  Created by Tiago  Santos on 24/03/2019.
//  Copyright © 2019 Tiago  Santos. All rights reserved.
//

import Foundation

class API{
	
	let apiUrl = "https://swapi.co/api/"
	
	func getCharacters (completion: @escaping (Any)->Void){
		
		var listOfCharacters: [Character] = []
		
		let url = URL(string: apiUrl + "people/")!
		var request = URLRequest(url: url)
		request.httpMethod =  "GET"
		
		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			if let httpResponse = response as? HTTPURLResponse {
				
				if (httpResponse.statusCode == 200){
					if data != nil {
						do{
							let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
							let jsonArray = json["results"]! as! [Any]
							
							if jsonArray.count != 0{
								
								for n in 0...jsonArray.count - 1 {
									
									var element = jsonArray[n]  as! [String: Any]
									let name = element["name"]! as? String ?? ""
									let skinColor = element["skin_color"]! as? String ?? ""
									let gender = element["gender"]! as? String ?? ""
									let speciesUrl = element["species"]! as? [String] ?? []
									let planetUrl = element["homeworld"]! as? String ?? ""
									let veiculesUrl = element["vehicles"]! as? [String] ?? []
									
									let character = Character(name: name, speciesUrl: speciesUrl, genre: gender, planetUrl: planetUrl, skinColor: skinColor, veiculesUrl: veiculesUrl, species: nil, veicules: nil, planet: nil)
									
									listOfCharacters.append(character)
								}
							}
							
						}catch{
							
						}
					}
				}
				DispatchQueue.main.async {
					completion(listOfCharacters)
				}
			}
			
		}
		task.resume()
	}
	
	
	func getCharacterSpecies(specieUrl: String, completion: @escaping (Any)->Void){
		
		var species: Species? = nil
		
		let url = URL(string: specieUrl)!
		var request = URLRequest(url: url)
		request.httpMethod =  "GET"
		
		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			if let httpResponse = response as? HTTPURLResponse {
				
				if (httpResponse.statusCode == 200){
					if data != nil {
						do{
							let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
							
								let name = json["name"]! as? String ?? ""
							
								let newSpecie = Species(name: name)
								species = newSpecie
						}catch{
							
						}
					}
				}
				DispatchQueue.main.async {
					completion(species as Any)
				}
			}
			
		}
		task.resume()
	}
	
	func getVehicles(vehiclesUrl: String, completion: @escaping (Any)->Void){
		var vehicle: Vehicle = Vehicle()
		
		let url = URL(string: vehiclesUrl)!
		var request = URLRequest(url: url)
		request.httpMethod =  "GET"
		
		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			if let httpResponse = response as? HTTPURLResponse {
				
				if (httpResponse.statusCode == 200){
					if data != nil {
						do{
							let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]

							let name = json["name"]! as? String ?? ""
							let model = json["model"]! as? String ?? ""
							let crew = json["crew"]! as? String ?? ""
							let passengers = json["passengers"]! as? String ?? ""
									
							vehicle = Vehicle(name: name, model: model, crew: crew, passengers: passengers)
							
						}catch{
							
						}
					}
				}
				DispatchQueue.main.async {
					completion(vehicle)
				}
			}
			
		}
		task.resume()
	}
	
	func getPlanet(planetUrl: String, completion: @escaping (Any)->Void){
		
		var planet = ""
		print(planetUrl)
		let url = URL(string: planetUrl)!
		var request = URLRequest(url: url)
		request.httpMethod =  "GET"
		
		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			if let httpResponse = response as? HTTPURLResponse {
				
				if (httpResponse.statusCode == 200){
					if data != nil {
						do{
							let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
							planet = json["name"]! as? String ?? ""
						}catch{
							
						}
					}
				}
				DispatchQueue.main.async {
					completion(planet)
				}
			}
			
		}
		task.resume()
	}

	
}
